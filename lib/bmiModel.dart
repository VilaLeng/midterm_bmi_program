import 'package:flutter/cupertino.dart';
class BMIModel{
  double bmi;
  bool isNormal;

  String comments;
  BMIModel({required this.bmi, required this.isNormal, required this.comments});
}