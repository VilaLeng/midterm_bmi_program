import 'package:bmi_usethis/bmiModel.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class ResultScreen extends StatelessWidget {
  final BMIModel bmiModel;

  ResultScreen(this.bmiModel);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.infinity,
        height: double.infinity,
        padding: EdgeInsets.all(32),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
          Container(
          height: 200,
          width: 200,
          child: bmiModel.isNormal
              ? SvgPicture.asset(
            "assets/images/happy.svg",
            fit: BoxFit.contain,
          )
              : SvgPicture.asset(
            "assets/images/sad.svg",
            fit: BoxFit.contain,
          ),
        ),

        SizedBox(
          height: 8,
        ),

        //แทรกข้อความ Your BMI is XXXX
        //แทรกข้อความ comments ใน bmiModel.comments
        Text(
          'Your BMI is ${bmiModel.bmi.round()}',
          style: TextStyle(
              color: Colors.red[700],
              fontWeight: FontWeight.w700,
              fontSize: 34),
        ),
            SizedBox(
              height: 16,
            ),

            Container(
              child: bmiModel.isNormal
                  ? Text(
                bmiModel.comments.toString(),
                style: TextStyle(
                  color: Colors.red[700],
                  fontWeight: FontWeight.w700,
                  fontSize: 18,
                ),
              )
                  : Text(bmiModel.comments.toString(),
                style: TextStyle(
                  color: Colors.red[700],
                  fontWeight: FontWeight.w700,
                  fontSize: 18,
                ),
              ),
            ),
        SizedBox(
          height: 16,
        ),

        Container(
            child: bmiModel.isNormal
                ? Text(
              'Hurry! Your BMI is Normal.',
              style: TextStyle(
                color: Colors.red[700],
                fontWeight: FontWeight.w700,
                fontSize: 18,
              ),
            )
                : Text('Sadly! Your BMI is not Normal',
              style: TextStyle(
              color: Colors.red[700],
              fontWeight: FontWeight.w700,
              fontSize: 18,
            ),
            ),
      ),
      //แทรกข้อความ Hurray! Your BMI is Normal. กรณีที่ bmiModel.isNormal เป็นจริง
      //หรือแทรกข้อความ Sadly! Your BMI is not Normal. กรณีที่ bmiModel.isNormal เป็นเท็จ
      //โดยใช้วิธีการกำหนดเงื่อนไขของ bmiModel.isNormal ดูตัวอย่าง บรรทัดที่ 24 ด้านบน

      SizedBox(
        height: 16,
      ),

      Container(
        child: ElevatedButton.icon(
          onPressed: () {
            //แทรกโค้ดให้คลิกแล้วกลับไปหน้าแรก
            Navigator.pop(context);
          },
          icon: Icon(
            Icons.arrow_back_ios,
            color: Colors.white,
          ),
          label: Text("LET CALCULATE AGAIN", ),
          style: ElevatedButton.styleFrom(primary: Colors.pink),
          //ปรับสีปุ่มให้เป็นสีชมพู
        ),
        width: double.infinity,
        padding: EdgeInsets.only(left: 16, right: 16),
      )
      ],
    ),)
    );
  }
}
